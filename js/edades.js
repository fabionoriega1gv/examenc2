let generar = document.getElementById('generar');
let limpiar = document.getElementById('limpiar');

let arregloEdades = new Array(100);

let bebe =0;
let niño =0;
let adolescente =0;
let adulto =0;
let anciano = 0;

let cantidadBebes = document.getElementById('cantidadBebes');
let cantidadNiños = document.getElementById('cantidadNiños');
let cantidadAdolescentes = document.getElementById('cantidadAdolescentes');
let cantidadAdultos = document.getElementById('cantidadAdultos');
let cantidadAncianos = document.getElementById('cantidadAncianos');

let input = document.getElementById('edades')
generar.addEventListener('click', function(){
    

for(let i=0; i < arregloEdades.length; i++){
    arregloEdades[i]= (Math.random()*100).toFixed(0);

    if(arregloEdades[i]>0 && arregloEdades[i]<4){
        bebe = bebe +1;
    }
    if(arregloEdades[i]>3 && arregloEdades[i]<13){
        niño = niño +1;
    }
    if(arregloEdades[i]>12 && arregloEdades[i]<18){
        adolescente = adolescente +1;
    }
    if(arregloEdades[i]>17 && arregloEdades[i]<61){
        adulto = adulto +1;
    }
    if(arregloEdades[i]>60 && arregloEdades[i]<101){
        anciano = anciano +1;
    }
}

let contenido = arregloEdades.map(String).join(", ")
input.value= contenido;

cantidadBebes.textContent = bebe;
cantidadNiños.textContent = niño;
cantidadAdolescentes.textContent = adolescente;
cantidadAdultos.textContent = adulto;
cantidadAncianos.textContent = anciano;
})

limpiar.addEventListener('click', function(){
    input.value=null;
    cantidadBebes.textContent = "";
    cantidadNiños.textContent = "";;
    cantidadAdolescentes.textContent = "";;
    cantidadAdultos.textContent = "";;
    cantidadAncianos.textContent = "";;  
})